-- migrate:up transaction:false

create unique index concurrently sub_post_comment_vote_cid_uid
  on sub_post_comment_vote (cid, uid);

-- migrate:down

drop index if exists sub_post_comment_vote_cid_uid;
