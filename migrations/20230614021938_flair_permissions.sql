-- migrate:up

-- Support restricting flairs to mods only.
alter table sub_flair add column mods_only boolean;
update sub_flair set mods_only = false;
alter table sub_flair alter column mods_only set not null;

-- Support restricting flairs to only certain post types.
-- A flair may not be used on a post type if a corresponding row
-- exists in this table.
create table if not exists disallow_flair_post_type (
  id serial not null primary key,
  flair_id integer not null,
  ptype integer not null,
  foreign key (flair_id) references sub_flair (xid));

create unique index if not exists disallowflairposttype_flairid_ptype
  on disallow_flair_post_type (flair_id, ptype);

grant select, insert, update, delete
  on table disallow_flair_post_type
  to throat_be;

grant select, insert, update, delete
  on table disallow_flair_post_type
  to throat_py;

-- migrate:down
drop table disallow_flair_post_type cascade;
alter table sub_flair drop column mods_only cascade;
