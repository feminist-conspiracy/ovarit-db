-- migrate:up

drop table sub_stylesheet cascade;

delete from site_metadata
 where key = 'storage.sub_css_max_file_size';

delete from user_metadata
 where key in ('nostyles', 'subtheme');

-- migrate:down

CREATE TABLE public.sub_stylesheet (
  xid serial NOT NULL PRIMARY KEY,
  content text,
  source text NOT NULL,
  sid character varying(40),
  FOREIGN KEY (sid) REFERENCES public.sub (sid)
);
CREATE INDEX substylesheet_sid ON public.sub_stylesheet (sid);
GRANT SELECT, INSERT, DELETE, UPDATE ON TABLE public.sub_stylesheet TO throat_be;
GRANT SELECT, INSERT, DELETE, UPDATE ON TABLE public.sub_stylesheet TO throat_py;

insert into site_metadata (key, value)
values ('storage.sub_css_max_file_size', '2');
