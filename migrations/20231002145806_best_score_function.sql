-- migrate:up

-- Calculate the lower bound of the Wilson score confidence interval
-- for a Bernoulli parameter.
create or replace function best_score(upvotes integer, downvotes integer, views integer)
  returns real as $$
  with
  vars as (
    select greatest(views, 1)::real as n,
           (upvotes - downvotes + 1)::real as score,
           1.96 as z
  ),
  calc as (
    select least(abs(score), n) / n as phat
      from vars
  )
  select ((phat
          + z * z / (2 * n)
          - z * sqrt((phat * (1 - phat) + z * z / (4 * n)) / n))
          / (1 + z * z / n)
          * sign(score)) :: real
  from vars, calc
$$ language sql immutable;

-- Ensure best_score returns expected values.
create function test_best_score() returns void AS $$
  declare num integer;
begin
  if best_score(1, 1, 1) != best_score(0, 0, 0) then
    raise exception 'best score self-voting check failed';
  end if;
  num = (select count(*)
           from (values
                 ( 1,  1,  1,  0.2065),
                 (20,  1, 25,  0.6087),
                 (20, 20, 25,  0.0071),
                 ( 1, 20, 25, -0.5242)
           ) as examples (upvotes, downvotes, views, score)
          where round(best_score(upvotes, downvotes, views) * 10000) != score * 10000);
  if num > 0 then
    raise exception 'example values check failed';
  end if;
end;
$$ language plpgsql;

select test_best_score();
drop function test_best_score;

alter table sub_post_comment
  drop column best_score;

-- migrate:down

alter table sub_post_comment
  add column best_score real not null
  default (0.206543);

update sub_post_comment
  set best_score = best_score(upvotes, downvotes, views);

drop function best_score(integer,integer,integer);
