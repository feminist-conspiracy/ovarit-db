-- migrate:up

-- Support changes to the Submit Post page
--  - different rules for each post type, per sub
--  - move the allow flag from sub_metadata to the new table

create table if not exists post_type_config (
  id serial not null primary key,
  sid varchar(40) not null,
  ptype integer not null,
  rules text default '' not null,
  mods_only boolean not null,
  foreign key (sid) references sub (sid));

create index if not exists posttypeconfig_sid
  on post_type_config (sid);
create unique index posttypeconfig_sid_ptype
  on post_type_config (sid, ptype);

grant select, insert, update, delete
  on table post_type_config
  to throat_be;

grant select, insert, update, delete
  on table post_type_config
  to throat_py;

-- Each sub has 4 entries in sub_metadata for the 4 post types.
-- Move these into the new table's mods_only field.
insert into post_type_config (sid, ptype, mods_only)
select sid,
       case when key = 'allow_text_posts' then 0
            when key = 'allow_link_posts' then 1
            when key = 'allow_upload_posts' then 2
            when key = 'allow_polls' then 3
       end as ptype,
       (value = '0') as mods_only
  from sub_metadata
 where key in ('allow_link_posts',
               'allow_text_posts',
               'allow_upload_posts',
               'allow_polls');

delete from sub_metadata
 where key in ('allow_link_posts',
               'allow_text_posts',
               'allow_upload_posts',
               'allow_polls');

-- migrate:down
insert into sub_metadata (sid, key, value)
select sid,
       case when ptype = 0 then 'allow_text_posts'
            when ptype = 1 then 'allow_link_posts'
            when ptype = 2 then 'allow_upload_posts'
            when ptype = 3 then 'allow_polls' end as key,
       case when mods_only then '0' else '1' end as value
  from post_type_config;

drop table if exists post_type_config cascade;
