-- migrate:up

alter table public.user add column upvotes_given integer;
alter table public.user add column downvotes_given integer;

-- migrate:down

alter table public.user drop column upvotes_given;
alter table public.user drop column downvotes_given;
