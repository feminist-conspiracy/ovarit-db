-- migrate:up
REVOKE CREATE ON SCHEMA public FROM public;
GRANT USAGE ON SCHEMA public TO public;

ALTER DEFAULT PRIVILEGES IN SCHEMA public GRANT SELECT, USAGE ON SEQUENCES TO throat_py;
ALTER DEFAULT PRIVILEGES IN SCHEMA public GRANT SELECT, USAGE ON SEQUENCES TO throat_be;
ALTER DEFAULT PRIVILEGES IN SCHEMA public GRANT SELECT, USAGE ON SEQUENCES TO ovarit_auth;
ALTER DEFAULT PRIVILEGES IN SCHEMA public GRANT SELECT, USAGE ON SEQUENCES TO ovarit_subs;

CREATE EXTENSION hll WITH SCHEMA public;

CREATE FUNCTION public.hot(score integer, date double precision) RETURNS numeric AS $$
  select round(cast(log(greatest(abs($1), 1)) * sign($1) + ($2 - 1134028003) / 45000.0 AS numeric), 7)
$$ LANGUAGE sql IMMUTABLE;

CREATE TABLE public.user (
  uid character varying(40) NOT NULL PRIMARY KEY,
  joindate timestamp without time zone,
  name character varying(64),
  score integer DEFAULT 0 NOT NULL,
  given integer DEFAULT 0 NOT NULL,
  status integer DEFAULT 0 NOT NULL,
  resets integer DEFAULT 0 NOT NULL,
  language character varying(11)
);
CREATE UNIQUE INDEX user_name ON public.user (name);
CREATE UNIQUE INDEX user_name_lower ON public.user (lower((name)::text));
GRANT SELECT, INSERT, DELETE, UPDATE ON TABLE public.user TO ovarit_auth;
GRANT SELECT, INSERT, DELETE, UPDATE ON TABLE public.user TO ovarit_subs;
GRANT SELECT, INSERT, DELETE, UPDATE ON TABLE public.user TO throat_be;
GRANT SELECT, INSERT, DELETE, UPDATE ON TABLE public.user TO throat_py;

CREATE TABLE public.sub (
  sid character varying(40) NOT NULL PRIMARY KEY,
  name character varying(32) NOT NULL,
  nsfw boolean DEFAULT false NOT NULL,
  sidebar text DEFAULT ''::text NOT NULL,
  status integer,
  title character varying(50),
  sort character varying(32),
  creation timestamp without time zone NOT NULL,
  subscribers integer DEFAULT 1 NOT NULL,
  posts integer DEFAULT 0 NOT NULL
);
CREATE UNIQUE INDEX sub_name ON public.sub (name);
CREATE UNIQUE INDEX sub_name_lower ON public.sub (lower(name));
GRANT SELECT, INSERT, DELETE, UPDATE ON TABLE public.sub TO ovarit_auth;
GRANT SELECT, INSERT, DELETE, UPDATE ON TABLE public.sub TO throat_be;
GRANT SELECT, INSERT, DELETE, UPDATE ON TABLE public.sub TO throat_py;

CREATE TABLE public.sub_post (
  pid serial NOT NULL PRIMARY KEY,
  content text,
  deleted integer NOT NULL,
  link character varying(255),
  nsfw boolean,
  posted timestamp without time zone NOT NULL,
  edited timestamp without time zone,
  ptype integer,
  score integer NOT NULL,
  upvotes integer DEFAULT 0 NOT NULL,
  downvotes integer DEFAULT 0 NOT NULL,
  sid character varying(40) NOT NULL,
  thumbnail character varying(255),
  title character varying(255) NOT NULL,
  comments integer NOT NULL,
  uid character varying(40) NOT NULL,
  flair character varying(25),
  distinguish integer,
  slug character varying(255) NOT NULL,
  FOREIGN KEY (sid) REFERENCES public.sub (sid),
  FOREIGN KEY (uid) REFERENCES public.user (uid)
);
CREATE INDEX sub_post_link ON public.sub_post (link);
CREATE INDEX sub_post_posted ON public.sub_post (posted);
CREATE INDEX subpost_sid ON public.sub_post (sid);
CREATE INDEX subpost_uid ON public.sub_post (uid);
GRANT SELECT, INSERT, DELETE, UPDATE ON TABLE public.sub_post TO throat_be;
GRANT SELECT, INSERT, DELETE, UPDATE ON TABLE public.sub_post TO throat_py;

CREATE TABLE public.sub_post_comment (
  cid character varying(40) NOT NULL PRIMARY KEY,
  content text,
  lastedit timestamp without time zone,
  parentcid character varying(40),
  pid integer,
  score integer,
  upvotes integer DEFAULT 0 NOT NULL,
  downvotes integer DEFAULT 0 NOT NULL,
  status integer,
  "time" timestamp without time zone,
  uid character varying(40),
  distinguish integer,
  best_score real,
  views integer NOT NULL,
  FOREIGN KEY (parentcid) REFERENCES public.sub_post_comment(cid),
  FOREIGN KEY (pid) REFERENCES public.sub_post(pid),
  FOREIGN KEY (uid) REFERENCES public.user (uid)
);
CREATE INDEX sub_post_comment_time ON public.sub_post_comment ("time");
CREATE INDEX subpostcomment_parentcid ON public.sub_post_comment (parentcid);
CREATE INDEX subpostcomment_pid ON public.sub_post_comment (pid);
CREATE INDEX subpostcomment_uid ON public.sub_post_comment (uid);
GRANT SELECT, INSERT, DELETE, UPDATE ON TABLE public.sub_post_comment TO throat_be;
GRANT SELECT, INSERT, DELETE, UPDATE ON TABLE public.sub_post_comment TO throat_py;

CREATE TABLE public.api_token (
  id serial NOT NULL PRIMARY KEY,
  uid character varying(40) NOT NULL,
  token character varying(255) NOT NULL,
  can_post boolean NOT NULL,
  can_mod boolean NOT NULL,
  can_message boolean NOT NULL,
  can_upload boolean NOT NULL,
  is_active boolean DEFAULT true NOT NULL,
  is_ip_restricted boolean DEFAULT false NOT NULL,
  FOREIGN KEY (uid) REFERENCES public.user (uid)
);
CREATE UNIQUE INDEX apitoken_token ON public.api_token (token);
CREATE INDEX apitoken_uid ON public.api_token (uid);
GRANT SELECT, INSERT, DELETE, UPDATE ON TABLE public.api_token TO throat_py;

CREATE TABLE public.api_token_settings (
  id serial NOT NULL PRIMARY KEY,
  token_id integer NOT NULL,
  key character varying(255) NOT NULL,
  value character varying(255) NOT NULL,
  FOREIGN KEY (token_id) REFERENCES public.api_token (id)
);
CREATE INDEX apitokensettings_token_id ON public.api_token_settings (token_id);
GRANT SELECT, INSERT, DELETE, UPDATE ON TABLE public.api_token_settings TO throat_py;

CREATE TABLE public.badge (
  bid serial NOT NULL PRIMARY KEY,
  name character varying(34) NOT NULL,
  alt character varying(255) NOT NULL,
  icon character varying(255) NOT NULL,
  score integer NOT NULL,
  rank integer NOT NULL,
  trigger character varying(255)
);
CREATE UNIQUE INDEX badge_name ON public.badge (name);
GRANT SELECT, INSERT, DELETE, UPDATE ON TABLE public.badge TO ovarit_subs;
GRANT SELECT, INSERT, DELETE, UPDATE ON TABLE public.badge TO throat_be;
GRANT SELECT, INSERT, DELETE, UPDATE ON TABLE public.badge TO throat_py;

CREATE TABLE public.sub_post_comment_report (
  id serial NOT NULL PRIMARY KEY,
  cid character varying(40) NOT NULL,
  uid character varying(40) NOT NULL,
  datetime timestamp without time zone NOT NULL,
  reason character varying(128) NOT NULL,
  open boolean NOT NULL,
  send_to_admin boolean NOT NULL,
  FOREIGN KEY (cid) REFERENCES public.sub_post_comment (cid),
  FOREIGN KEY (uid) REFERENCES public.user (uid)
);
CREATE INDEX sub_post_comment_report_datetime ON public.sub_post_comment_report (datetime);
CREATE INDEX subpostcommentreport_cid ON public.sub_post_comment_report (cid);
CREATE INDEX subpostcommentreport_uid ON public.sub_post_comment_report (uid);
GRANT SELECT, INSERT, DELETE, UPDATE ON TABLE public.sub_post_comment_report TO throat_be;
GRANT SELECT, INSERT, DELETE, UPDATE ON TABLE public.sub_post_comment_report TO throat_py;

CREATE TABLE public.comment_report_log (
  lid serial NOT NULL PRIMARY KEY,
  id integer NOT NULL,
  action integer,
  "desc" character varying(255),
  link character varying(255),
  "time" timestamp without time zone NOT NULL,
  uid character varying(40),
  target_uid character varying(40),
  FOREIGN KEY (id) REFERENCES public.sub_post_comment_report (id),
  FOREIGN KEY (target_uid) REFERENCES public.user (uid),
  FOREIGN KEY (uid) REFERENCES public.user (uid)
);
CREATE INDEX commentreportlog_id ON public.comment_report_log (id);
CREATE INDEX commentreportlog_target_uid ON public.comment_report_log (target_uid);
CREATE INDEX commentreportlog_uid ON public.comment_report_log (uid);
GRANT SELECT, INSERT, DELETE, UPDATE ON TABLE public.comment_report_log TO throat_be;
GRANT SELECT, INSERT, DELETE, UPDATE ON TABLE public.comment_report_log TO throat_py;

CREATE TABLE public.customer_update_log (
  id serial NOT NULL PRIMARY KEY,
  uid character varying(40) NOT NULL,
  action text NOT NULL,
  value text,
  created timestamp without time zone NOT NULL,
  completed timestamp without time zone,
  success boolean,
  FOREIGN KEY (uid) REFERENCES public.user (uid)
);
CREATE INDEX customerupdatelog_uid ON public.customer_update_log (uid);
GRANT SELECT, INSERT, DELETE, UPDATE ON TABLE public.customer_update_log TO ovarit_auth;
GRANT SELECT, INSERT, DELETE, UPDATE ON TABLE public.customer_update_log TO throat_be;
GRANT SELECT, INSERT, DELETE, UPDATE ON TABLE public.customer_update_log TO throat_py;

CREATE TABLE public.daily_uniques (
  date date UNIQUE,
  ip_hashes public.hll
);
GRANT SELECT, INSERT, DELETE, UPDATE ON TABLE public.daily_uniques TO throat_be;

CREATE TABLE public.invite_code (
  id serial NOT NULL PRIMARY KEY,
  uid character varying(40) NOT NULL,
  created timestamp without time zone NOT NULL,
  code character varying(64) NOT NULL,
  expires timestamp without time zone,
  uses integer NOT NULL,
  max_uses integer NOT NULL,
  FOREIGN KEY (uid) REFERENCES public.user (uid));
CREATE INDEX invite_code_code ON public.invite_code (code);
CREATE INDEX invitecode_uid ON public.invite_code (uid);
GRANT SELECT, INSERT, DELETE, UPDATE ON TABLE public.invite_code TO ovarit_auth;
GRANT SELECT, INSERT, DELETE, UPDATE ON TABLE public.invite_code TO throat_be;
GRANT SELECT, INSERT, DELETE, UPDATE ON TABLE public.invite_code TO throat_py;

CREATE TABLE public.message_thread (
  mtid serial NOT NULL PRIMARY KEY,
  replies integer NOT NULL,
  subject character varying(255) NOT NULL,
  sid character varying(40),
  FOREIGN KEY (sid) REFERENCES public.sub (sid)
);
CREATE INDEX messagethread_sid ON public.message_thread (sid);
GRANT SELECT, INSERT, DELETE, UPDATE ON TABLE public.message_thread TO throat_be;
GRANT SELECT, INSERT, DELETE, UPDATE ON TABLE public.message_thread TO throat_py;

CREATE TABLE public.message (
  mid serial NOT NULL PRIMARY KEY,
  content text,
  mtype integer,
  posted timestamp without time zone,
  receivedby character varying(40),
  sentby character varying(40),
  first boolean NOT NULL,
  mtid integer NOT NULL,
  FOREIGN KEY (mtid) REFERENCES public.message_thread (mtid),
  FOREIGN KEY (receivedby) REFERENCES public.user (uid),
  FOREIGN KEY (sentby) REFERENCES public.user (uid)
);
CREATE INDEX message_mtid ON public.message (mtid);
CREATE INDEX message_posted ON public.message (posted);
CREATE INDEX message_receivedby ON public.message (receivedby);
CREATE INDEX message_sentby ON public.message (sentby);
GRANT SELECT, INSERT, DELETE, UPDATE ON TABLE public.message TO throat_be;
GRANT SELECT, INSERT, DELETE, UPDATE ON TABLE public.message TO throat_py;

CREATE TABLE public.notification (
  id serial NOT NULL PRIMARY KEY,
  type character varying(255) NOT NULL,
  sid character varying(40),
  pid integer,
  cid character varying(40),
  sentby character varying(40),
  receivedby character varying(40),
  read timestamp without time zone,
  content text,
  created timestamp without time zone NOT NULL,
  FOREIGN KEY (cid) REFERENCES public.sub_post_comment (cid),
  FOREIGN KEY (pid) REFERENCES public.sub_post (pid),
  FOREIGN KEY (receivedby) REFERENCES public.user (uid),
  FOREIGN KEY (sentby) REFERENCES public.user (uid),
  FOREIGN KEY (sid) REFERENCES public.sub (sid)
);
CREATE INDEX notification_cid ON public.notification (cid);
CREATE INDEX notification_pid ON public.notification (pid);
CREATE INDEX notification_receivedby ON public.notification (receivedby);
CREATE INDEX notification_sentby ON public.notification (sentby);
CREATE INDEX notification_sid ON public.notification (sid);
GRANT SELECT, INSERT, DELETE, UPDATE ON TABLE public.notification TO throat_be;
GRANT SELECT, INSERT, DELETE, UPDATE ON TABLE public.notification TO throat_py;

CREATE TABLE public.payments (
  payment_id character varying(40) NOT NULL PRIMARY KEY,
  uid character varying(40) NOT NULL,
  paid boolean NOT NULL,
  amount integer NOT NULL,
  date timestamp without time zone NOT NULL,
  FOREIGN KEY (uid) REFERENCES public.user (uid)
);
CREATE INDEX payments_uid ON public.payments (uid);
GRANT SELECT, INSERT, DELETE, UPDATE ON TABLE public.payments TO ovarit_subs;

CREATE TABLE public.sub_post_report (
  id serial NOT NULL PRIMARY KEY,
  pid integer NOT NULL,
  uid character varying(40) NOT NULL,
  datetime timestamp without time zone NOT NULL,
  reason character varying(128) NOT NULL,
  open boolean NOT NULL,
  send_to_admin boolean NOT NULL,
  FOREIGN KEY (pid) REFERENCES public.sub_post (pid),
  FOREIGN KEY (uid) REFERENCES public.user (uid)
);
CREATE INDEX sub_post_report_datetime ON public.sub_post_report (datetime);
CREATE INDEX subpostreport_pid ON public.sub_post_report (pid);
CREATE INDEX subpostreport_uid ON public.sub_post_report (uid);
GRANT SELECT, INSERT, DELETE, UPDATE ON TABLE public.sub_post_report TO throat_be;
GRANT SELECT, INSERT, DELETE, UPDATE ON TABLE public.sub_post_report TO throat_py;

CREATE TABLE public.post_report_log (
  lid serial NOT NULL PRIMARY KEY,
  id integer NOT NULL,
  action integer,
  "desc" character varying(255),
  link character varying(255),
  "time" timestamp without time zone NOT NULL,
  uid character varying(40),
  target_uid character varying(40),
  FOREIGN KEY (id) REFERENCES public.sub_post_report (id),
  FOREIGN KEY (target_uid) REFERENCES public.user (uid),
  FOREIGN KEY (uid) REFERENCES public.user (uid)
);
GRANT SELECT, INSERT, DELETE, UPDATE ON TABLE public.post_report_log TO throat_be;
GRANT SELECT, INSERT, DELETE, UPDATE ON TABLE public.post_report_log TO throat_py;

CREATE TABLE public.site_log (
  lid serial NOT NULL PRIMARY KEY,
  action integer,
  "desc" character varying(255),
  link character varying(255),
  "time" timestamp without time zone NOT NULL,
  uid character varying(40),
  target_uid character varying(40),
  FOREIGN KEY (target_uid) REFERENCES public.user (uid),
  FOREIGN KEY (uid) REFERENCES public.user (uid)
);
CREATE INDEX sitelog_target_uid ON public.site_log (target_uid);
CREATE INDEX sitelog_uid ON public.site_log (uid);
GRANT SELECT, INSERT, DELETE, UPDATE ON TABLE public.site_log TO throat_be;
GRANT SELECT, INSERT, DELETE, UPDATE ON TABLE public.site_log TO throat_py;

CREATE TABLE public.site_metadata (
  xid serial NOT NULL PRIMARY KEY,
  key character varying(255),
  value character varying(255)
);
GRANT SELECT, INSERT, DELETE, UPDATE ON TABLE public.site_metadata TO ovarit_auth;
GRANT SELECT, INSERT, DELETE, UPDATE ON TABLE public.site_metadata TO throat_be;
GRANT SELECT, INSERT, DELETE, UPDATE ON TABLE public.site_metadata TO throat_py;
INSERT INTO public.site_metadata (key, value)
VALUES ('site.name', 'Throat'),
       ('site.lema', 'Throat: Open discussion ;D'),
       ('site.copyright', 'Umbrella Corp'),
       ('site.placeholder_account', 'Site'),
       ('site.allow_uploads', '0'),
       ('site.allow_video_uploads', '1'),
       ('site.upload_min_level', '0'),
       ('site.enable_chat', '1'),
       ('site.sitelog_public', '1'),
       ('site.force_sublog_public', '1'),
       ('site.front_page_submit', '1'),
       ('site.block_anon_stalking', '0'),
       ('site.changelog_sub', ''),
       ('site.title_edit_timeout', '300'),
       ('site.sub_creation_min_level', '2'),
       ('site.sub_creation_admin_only', '0'),
       ('site.sub_ownership_limit', '20'),
       ('site.edit_history', '0'),
       ('site.anonymous_modding', '0'),
       ('site.send_pm_to_user_min_level', '3'),
       ('site.daily_sub_posting_limit', '10'),
       ('site.daily_site_posting_limit', '25'),
       ('site.archive_post_after', '60'),
       ('site.recent_activity.enabled', '0'),
       ('site.recent_activity.defaults_only', '0'),
       ('site.recent_activity.comments_only', '0'),
       ('site.recent_activity.max_entries', '10'),
       ('storage.sub_css_max_file_size', '2'),
       ('site.enable_posting', '1'),
       ('site.enable_registration', '1'),
       ('site.invitations_visible_to_users', '0'),
       ('site.invite_level', '3'),
       ('site.invite_max', '10'),
       ('site.require_invite_code', '0'),
       ('site.notifications_on_icon', '1'),
       ('site.archive_sticky_posts', '1'),
       ('site.nsfw.anon.show', '1'),
       ('site.nsfw.anon.blur', '1'),
       ('site.nsfw.new_user_default.show', '1'),
       ('site.nsfw.new_user_default.blur', '1'),
       ('site.username_max_length', '32'),
       ('site.enable_modmail', '0'),
       ('site.show_votes', '1'),
       ('site.top_posts.show_score', '1'),
       ('site.self_voting.posts', '1'),
       ('site.self_voting.comments', '0'),
       ('site.description', 'A link aggregator and discussion platform'),
       ('sub_post_view_init', now()),
       ('best_comment_sort_init', now());

CREATE TABLE public.sub_ban (
  id serial NOT NULL PRIMARY KEY,
  uid character varying(40) NOT NULL,
  sid character varying(40) NOT NULL,
  created timestamp without time zone,
  reason character varying(128) NOT NULL,
  expires timestamp without time zone,
  effective boolean NOT NULL,
  created_by_id character varying(40),
  FOREIGN KEY (created_by_id) REFERENCES public.user (uid),
  FOREIGN KEY (sid) REFERENCES public.sub (sid),
  FOREIGN KEY (uid) REFERENCES public.user (uid)
);
CREATE INDEX subban_created_by_id ON public.sub_ban (created_by_id);
CREATE INDEX subban_sid ON public.sub_ban (sid);
CREATE INDEX subban_uid ON public.sub_ban (uid);
GRANT SELECT, INSERT, DELETE, UPDATE ON TABLE public.sub_ban TO throat_be;
GRANT SELECT, INSERT, DELETE, UPDATE ON TABLE public.sub_ban TO throat_py;

CREATE TABLE public.sub_flair (
  xid serial NOT NULL PRIMARY KEY,
  sid character varying(40),
  text character varying(255),
  "order" integer,
  FOREIGN KEY (sid) REFERENCES public.sub (sid)
);
CREATE INDEX subflair_sid ON public.sub_flair (sid);
GRANT SELECT, INSERT, DELETE, UPDATE ON TABLE public.sub_flair TO throat_be;
GRANT SELECT, INSERT, DELETE, UPDATE ON TABLE public.sub_flair TO throat_py;

CREATE TABLE public.sub_log (
  lid serial NOT NULL PRIMARY KEY,
  action integer,
  "desc" character varying(255),
  link character varying(255),
  sid character varying(40),
  uid character varying(40),
  target_uid character varying(40),
  admin boolean DEFAULT false NOT NULL,
  "time" timestamp without time zone NOT NULL,
  FOREIGN KEY (sid) REFERENCES public.sub (sid),
  FOREIGN KEY (target_uid) REFERENCES public.user (uid),
  FOREIGN KEY (uid) REFERENCES public.user (uid)
);
CREATE INDEX sublog_sid ON public.sub_log (sid);
CREATE INDEX sublog_target_uid ON public.sub_log (target_uid);
CREATE INDEX sublog_uid ON public.sub_log (uid);
GRANT SELECT, INSERT, DELETE, UPDATE ON TABLE public.sub_log TO throat_be;
GRANT SELECT, INSERT, DELETE, UPDATE ON TABLE public.sub_log TO throat_py;

CREATE TABLE public.sub_message_log (
  id serial NOT NULL PRIMARY KEY,
  uid character varying(40) NOT NULL,
  updated timestamp without time zone,
  action integer NOT NULL,
  "desc" character varying(255),
  mtid integer,
  FOREIGN KEY (mtid) REFERENCES public.message_thread (mtid),
  FOREIGN KEY (uid) REFERENCES public.user (uid)
);
CREATE INDEX sub_message_log_mtid ON public.sub_message_log (mtid);
CREATE INDEX sub_message_log_updated ON public.sub_message_log (updated);
CREATE INDEX submessagelog_uid ON public.sub_message_log (uid);
GRANT SELECT, INSERT, DELETE, UPDATE ON TABLE public.sub_message_log TO throat_be;
GRANT SELECT, INSERT, DELETE, UPDATE ON TABLE public.sub_message_log TO throat_py;

CREATE TABLE public.sub_message_mailbox (
  id serial NOT NULL PRIMARY KEY,
  mailbox integer NOT NULL,
  highlighted boolean NOT NULL,
  mtid integer NOT NULL,
  FOREIGN KEY (mtid) REFERENCES public.message_thread (mtid)
);
CREATE INDEX sub_message_mailbox_mtid ON public.sub_message_mailbox (mtid);
GRANT SELECT, INSERT, DELETE, UPDATE ON TABLE public.sub_message_mailbox TO throat_be;
GRANT SELECT, INSERT, DELETE, UPDATE ON TABLE public.sub_message_mailbox TO throat_py;

CREATE TABLE public.sub_metadata (
  xid serial NOT NULL PRIMARY KEY,
  key character varying(255),
  sid character varying(40),
  value character varying(255),
  FOREIGN KEY (sid) REFERENCES public.sub (sid)
);
CREATE INDEX submetadata_sid ON public.sub_metadata (sid);
GRANT SELECT, INSERT, DELETE, UPDATE ON TABLE public.sub_metadata TO throat_be;
GRANT SELECT, INSERT, DELETE, UPDATE ON TABLE public.sub_metadata TO throat_py;

CREATE TABLE public.sub_mod (
  id serial NOT NULL PRIMARY KEY,
  uid character varying(40) NOT NULL,
  sid character varying(40) NOT NULL,
  power_level integer NOT NULL,
  invite boolean NOT NULL,
  FOREIGN KEY (sid) REFERENCES public.sub (sid),
  FOREIGN KEY (uid) REFERENCES public.user (uid)
);
CREATE INDEX submod_sid ON public.sub_mod (sid);
CREATE INDEX submod_uid ON public.sub_mod (uid);
GRANT SELECT, INSERT, DELETE, UPDATE ON TABLE public.sub_mod TO throat_be;
GRANT SELECT, INSERT, DELETE, UPDATE ON TABLE public.sub_mod TO throat_py;

CREATE TABLE public.sub_post_comment_checkoff (
  id serial NOT NULL PRIMARY KEY,
  cid character varying(40) NOT NULL,
  uid character varying(40) NOT NULL,
  datetime timestamp without time zone NOT NULL,
  FOREIGN KEY (cid) REFERENCES public.sub_post_comment (cid),
  FOREIGN KEY (uid) REFERENCES public.user (uid)
);
CREATE UNIQUE INDEX subpostcommentcheckoff_cid ON public.sub_post_comment_checkoff (cid);
CREATE INDEX subpostcommentcheckoff_uid ON public.sub_post_comment_checkoff (uid);
GRANT SELECT, INSERT, DELETE, UPDATE ON TABLE public.sub_post_comment_checkoff TO throat_be;
GRANT SELECT, INSERT, DELETE, UPDATE ON TABLE public.sub_post_comment_checkoff TO throat_py;

CREATE TABLE public.sub_post_comment_history (
  id serial NOT NULL PRIMARY KEY,
  cid character varying(40) NOT NULL,
  datetime timestamp without time zone NOT NULL,
  content text,
  FOREIGN KEY (cid) REFERENCES public.sub_post_comment (cid)
);
CREATE INDEX subpostcommenthistory_cid ON public.sub_post_comment_history (cid);
GRANT SELECT, INSERT, DELETE, UPDATE ON TABLE public.sub_post_comment_history TO throat_be;
GRANT SELECT, INSERT, DELETE, UPDATE ON TABLE public.sub_post_comment_history TO throat_py;

CREATE TABLE public.sub_post_comment_view (
  id serial NOT NULL PRIMARY KEY,
  cid character varying(40) NOT NULL,
  uid character varying(40) NOT NULL,
  pid integer NOT NULL,
  FOREIGN KEY (cid) REFERENCES public.sub_post_comment (cid),
  FOREIGN KEY (pid) REFERENCES public.sub_post (pid),
  FOREIGN KEY (uid) REFERENCES public.user (uid)
);
CREATE INDEX subpostcommentview_cid ON public.sub_post_comment_view (cid);
CREATE UNIQUE INDEX subpostcommentview_cid_uid ON public.sub_post_comment_view (cid, uid);
CREATE INDEX subpostcommentview_pid ON public.sub_post_comment_view (pid);
CREATE INDEX subpostcommentview_uid ON public.sub_post_comment_view (uid);
GRANT SELECT, INSERT, DELETE, UPDATE ON TABLE public.sub_post_comment_view TO throat_be;
GRANT SELECT, INSERT, DELETE, UPDATE ON TABLE public.sub_post_comment_view TO throat_py;

CREATE TABLE public.sub_post_comment_vote (
  xid serial NOT NULL PRIMARY KEY,
  datetime timestamp without time zone,
  cid character varying(255),
  positive integer,
  uid character varying(40),
  FOREIGN KEY (uid) REFERENCES public.user (uid)
);
CREATE INDEX sub_post_comment_vote_cid ON public.sub_post_comment_vote (cid);
CREATE INDEX sub_post_comment_vote_datetime ON public.sub_post_comment_vote (datetime);
CREATE INDEX subpostcommentvote_uid ON public.sub_post_comment_vote (uid);
GRANT SELECT, INSERT, DELETE, UPDATE ON TABLE public.sub_post_comment_vote TO throat_be;
GRANT SELECT, INSERT, DELETE, UPDATE ON TABLE public.sub_post_comment_vote TO throat_py;

CREATE TABLE public.sub_post_content_history (
  id serial NOT NULL PRIMARY KEY,
  pid integer,
  content text,
  datetime timestamp without time zone NOT NULL,
  FOREIGN KEY (pid) REFERENCES public.sub_post (pid)
);
CREATE INDEX subpostcontenthistory_pid ON public.sub_post_content_history (pid);
GRANT SELECT, INSERT, DELETE, UPDATE ON TABLE public.sub_post_content_history TO throat_be;
GRANT SELECT, INSERT, DELETE, UPDATE ON TABLE public.sub_post_content_history TO throat_py;

CREATE TABLE public.sub_post_metadata (
  xid serial NOT NULL PRIMARY KEY,
  key character varying(255),
  pid integer,
  value character varying(255),
  FOREIGN KEY (pid) REFERENCES public.sub_post (pid)
);
CREATE INDEX subpostmetadata_pid ON public.sub_post_metadata (pid);
GRANT SELECT, INSERT, DELETE, UPDATE ON TABLE public.sub_post_metadata TO throat_be;
GRANT SELECT, INSERT, DELETE, UPDATE ON TABLE public.sub_post_metadata TO throat_py;

CREATE TABLE public.sub_post_poll_option (
  id serial NOT NULL PRIMARY KEY,
  pid integer NOT NULL,
  text character varying(255) NOT NULL,
  FOREIGN KEY (pid) REFERENCES public.sub_post (pid)
);
CREATE INDEX subpostpolloption_pid ON public.sub_post_poll_option (pid);
GRANT SELECT, INSERT, DELETE, UPDATE ON TABLE public.sub_post_poll_option TO throat_be;
GRANT SELECT, INSERT, DELETE, UPDATE ON TABLE public.sub_post_poll_option TO throat_py;

CREATE TABLE public.sub_post_poll_vote (
  id serial NOT NULL PRIMARY KEY,
  pid integer NOT NULL,
  uid character varying(40) NOT NULL,
  vid integer NOT NULL,
  FOREIGN KEY (pid) REFERENCES public.sub_post (pid),
  FOREIGN KEY (uid) REFERENCES public.user (uid),
  FOREIGN KEY (vid) REFERENCES public.sub_post_poll_option (id)
);
CREATE INDEX subpostpollvote_pid ON public.sub_post_poll_vote (pid);
CREATE INDEX subpostpollvote_uid ON public.sub_post_poll_vote (uid);
CREATE INDEX subpostpollvote_vid ON public.sub_post_poll_vote (vid);
GRANT SELECT, INSERT, DELETE, UPDATE ON TABLE public.sub_post_poll_vote TO throat_be;
GRANT SELECT, INSERT, DELETE, UPDATE ON TABLE public.sub_post_poll_vote TO throat_py;

CREATE TABLE public.sub_post_title_history (
  id serial NOT NULL PRIMARY KEY,
  pid integer,
  title text,
  datetime timestamp without time zone NOT NULL,
  uid character varying(40) NOT NULL,
  FOREIGN KEY (pid) REFERENCES public.sub_post (pid),
  FOREIGN KEY (uid) REFERENCES public.user (uid)
);
CREATE INDEX subposttitlehistory_pid ON public.sub_post_title_history (pid);
CREATE INDEX sub_post_title_history_uid ON sub_post_title_history (uid);
GRANT SELECT, INSERT, DELETE, UPDATE ON TABLE public.sub_post_title_history TO throat_be;
GRANT SELECT, INSERT, DELETE, UPDATE ON TABLE public.sub_post_title_history TO throat_py;

CREATE TABLE public.sub_post_view (
  id serial NOT NULL PRIMARY KEY,
  uid character varying(40) NOT NULL,
  pid integer NOT NULL,
  datetime timestamp without time zone,
  FOREIGN KEY (pid) REFERENCES public.sub_post (pid),
  FOREIGN KEY (uid) REFERENCES public.user (uid)
);
CREATE INDEX subpostview_pid ON public.sub_post_view (pid);
CREATE UNIQUE INDEX subpostview_pid_uid ON public.sub_post_view (pid, uid);
CREATE INDEX subpostview_uid ON public.sub_post_view (uid);
GRANT SELECT, INSERT, DELETE, UPDATE ON TABLE public.sub_post_view TO throat_be;
GRANT SELECT, INSERT, DELETE, UPDATE ON TABLE public.sub_post_view TO throat_py;

CREATE TABLE public.sub_post_vote (
  xid serial NOT NULL PRIMARY KEY,
  datetime timestamp without time zone,
  pid integer,
  positive integer,
  uid character varying(40),
  FOREIGN KEY (pid) REFERENCES public.sub_post (pid),
  FOREIGN KEY (uid) REFERENCES public.user (uid)
);
CREATE INDEX sub_post_vote_datetime ON public.sub_post_vote (datetime);
CREATE INDEX subpostvote_pid ON public.sub_post_vote (pid);
CREATE INDEX subpostvote_uid ON public.sub_post_vote (uid);
GRANT SELECT, INSERT, DELETE, UPDATE ON TABLE public.sub_post_vote TO throat_be;
GRANT SELECT, INSERT, DELETE, UPDATE ON TABLE public.sub_post_vote TO throat_py;

CREATE TABLE public.sub_rule (
  rid serial NOT NULL PRIMARY KEY,
  sid character varying(40),
  text character varying(255),
  "order" integer,
  FOREIGN KEY (sid) REFERENCES public.sub (sid)
);
CREATE INDEX subrule_sid ON public.sub_rule (sid);
GRANT SELECT, INSERT, DELETE, UPDATE ON TABLE public.sub_rule TO throat_be;
GRANT SELECT, INSERT, DELETE, UPDATE ON TABLE public.sub_rule TO throat_py;

CREATE TABLE public.sub_stylesheet (
  xid serial NOT NULL PRIMARY KEY,
  content text,
  source text NOT NULL,
  sid character varying(40),
  FOREIGN KEY (sid) REFERENCES public.sub (sid)
);
CREATE INDEX substylesheet_sid ON public.sub_stylesheet (sid);
GRANT SELECT, INSERT, DELETE, UPDATE ON TABLE public.sub_stylesheet TO throat_be;
GRANT SELECT, INSERT, DELETE, UPDATE ON TABLE public.sub_stylesheet TO throat_py;

CREATE TABLE public.sub_subscriber (
  xid serial NOT NULL PRIMARY KEY,
  "order" integer,
  sid character varying(40),
  status integer,
  "time" timestamp without time zone NOT NULL,
  uid character varying(40),
  FOREIGN KEY (sid) REFERENCES public.sub (sid),
  FOREIGN KEY (uid) REFERENCES public.user (uid)
);
CREATE INDEX subsubscriber_sid ON public.sub_subscriber (sid);
CREATE INDEX subsubscriber_uid ON public.sub_subscriber (uid);
GRANT SELECT, INSERT, DELETE, UPDATE ON TABLE public.sub_subscriber TO ovarit_auth;
GRANT SELECT, INSERT, DELETE, UPDATE ON TABLE public.sub_subscriber TO throat_be;
GRANT SELECT, INSERT, DELETE, UPDATE ON TABLE public.sub_subscriber TO throat_py;

CREATE TABLE public.sub_uploads (
  id serial NOT NULL PRIMARY KEY,
  sid character varying(40) NOT NULL,
  fileid character varying(255) NOT NULL,
  thumbnail character varying(255) NOT NULL,
  name character varying(255) NOT NULL,
  size integer NOT NULL,
  FOREIGN KEY (sid) REFERENCES public.sub (sid)
);
CREATE INDEX subuploads_sid ON public.sub_uploads (sid);
GRANT SELECT, INSERT, DELETE, UPDATE ON TABLE public.sub_uploads TO throat_be;
GRANT SELECT, INSERT, DELETE, UPDATE ON TABLE public.sub_uploads TO throat_py;

CREATE TABLE public.sub_user_flair_choice (
  id serial NOT NULL PRIMARY KEY,
  sid character varying(40) NOT NULL,
  flair character varying(25) NOT NULL,
  FOREIGN KEY (sid) REFERENCES public.sub (sid)
);
CREATE INDEX subuserflairchoice_sid ON public.sub_user_flair_choice (sid);
GRANT SELECT, INSERT, DELETE, UPDATE ON TABLE public.sub_user_flair_choice TO throat_be;
GRANT SELECT, INSERT, DELETE, UPDATE ON TABLE public.sub_user_flair_choice TO throat_py;

CREATE TABLE public.sub_user_flair (
  id serial NOT NULL PRIMARY KEY,
  uid character varying(40) NOT NULL,
  sid character varying(40) NOT NULL,
  flair character varying(25) NOT NULL,
  flair_choice_id integer,
  FOREIGN KEY (flair_choice_id) REFERENCES public.sub_user_flair_choice (id),
  FOREIGN KEY (sid) REFERENCES public.sub (sid),
  FOREIGN KEY (uid) REFERENCES public.user (uid)
);
CREATE INDEX subuserflair_sid ON public.sub_user_flair (sid);
CREATE INDEX subuserflair_uid ON public.sub_user_flair (uid);
CREATE INDEX subuserflair_flair_choice_id ON public.sub_user_flair (flair_choice_id);
GRANT SELECT, INSERT, DELETE, UPDATE ON TABLE public.sub_user_flair TO throat_be;
GRANT SELECT, INSERT, DELETE, UPDATE ON TABLE public.sub_user_flair TO throat_py;

CREATE TABLE public.user_content_block (
  id serial NOT NULL PRIMARY KEY,
  uid character varying(40) NOT NULL,
  target character varying(40) NOT NULL,
  date timestamp without time zone NOT NULL,
  method integer NOT NULL,
  FOREIGN KEY (uid) REFERENCES public.user (uid)
);
CREATE INDEX user_content_block_target ON public.user_content_block (target);
CREATE INDEX usercontentblock_uid ON public.user_content_block (uid);
GRANT SELECT, INSERT, DELETE, UPDATE ON TABLE public.user_content_block TO throat_be;
GRANT SELECT, INSERT, DELETE, UPDATE ON TABLE public.user_content_block TO throat_py;

CREATE TABLE public.user_login (
  id serial NOT NULL PRIMARY KEY,
  uid character varying(40) NOT NULL,
  email character varying(255) NOT NULL,
  parsed_hash character varying(255) NOT NULL,
  pending_action jsonb,
  FOREIGN KEY (uid) REFERENCES public.user (uid)
);
CREATE INDEX IF NOT EXISTS userlogin_uid ON user_login (uid);
CREATE INDEX userlogin_code_verify ON user_login ((pending_action->'Verify'->>'code'));
CREATE INDEX userlogin_code_reset ON user_login ((pending_action->'Reset'->>'code'));
CREATE INDEX userlogin_code_newemail ON user_login ((pending_action->'NewEmail'->>'code'));
GRANT SELECT, INSERT, UPDATE, DELETE ON TABLE user_login TO ovarit_auth;

CREATE TABLE public.user_message_block (
  id serial NOT NULL PRIMARY KEY,
  uid character varying(40) NOT NULL,
  target character varying(40) NOT NULL,
  date timestamp without time zone NOT NULL,
  FOREIGN KEY (uid) REFERENCES public.user (uid)
);
CREATE INDEX user_message_block_target ON public.user_message_block (target);
CREATE INDEX usermessageblock_uid ON public.user_message_block (uid);
GRANT SELECT, INSERT, DELETE, UPDATE ON TABLE public.user_message_block TO throat_be;
GRANT SELECT, INSERT, DELETE, UPDATE ON TABLE public.user_message_block TO throat_py;

CREATE TABLE public.user_message_mailbox (
  id serial NOT NULL PRIMARY KEY,
  uid character varying(40) NOT NULL,
  mid integer NOT NULL,
  mailbox integer NOT NULL,
  FOREIGN KEY (mid) REFERENCES public.message (mid),
  FOREIGN KEY (uid) REFERENCES public.user (uid)
);
CREATE INDEX usermessagemailbox_mid ON public.user_message_mailbox (mid);
CREATE INDEX usermessagemailbox_uid ON public.user_message_mailbox (uid);
GRANT SELECT, INSERT, DELETE, UPDATE ON TABLE public.user_message_mailbox TO throat_be;
GRANT SELECT, INSERT, DELETE, UPDATE ON TABLE public.user_message_mailbox TO throat_py;

CREATE TABLE public.user_metadata (
  xid serial NOT NULL PRIMARY KEY,
  key character varying(255),
  uid character varying(40),
  value character varying(255),
  FOREIGN KEY (uid) REFERENCES public.user (uid)
);
CREATE INDEX usermetadata_uid ON public.user_metadata (uid);
GRANT SELECT, INSERT, DELETE, UPDATE ON TABLE public.user_metadata TO ovarit_auth;
GRANT SELECT, INSERT, DELETE, UPDATE ON TABLE public.user_metadata TO ovarit_subs;
GRANT SELECT, INSERT, DELETE, UPDATE ON TABLE public.user_metadata TO throat_be;
GRANT SELECT, INSERT, DELETE, UPDATE ON TABLE public.user_metadata TO throat_py;

CREATE TABLE public.user_saved (
  xid serial NOT NULL PRIMARY KEY,
  pid integer,
  uid character varying(40),
  FOREIGN KEY (uid) REFERENCES public.user (uid)
);
CREATE INDEX usersaved_uid ON public.user_saved (uid);
GRANT SELECT, INSERT, DELETE, UPDATE ON TABLE public.user_saved TO throat_be;
GRANT SELECT, INSERT, DELETE, UPDATE ON TABLE public.user_saved TO throat_py;

CREATE TABLE public.user_unread_message (
  id serial NOT NULL PRIMARY KEY,
  uid character varying(40) NOT NULL,
  mid integer NOT NULL,
  FOREIGN KEY (mid) REFERENCES public.message (mid),
  FOREIGN KEY (uid) REFERENCES public.user (uid)
);
CREATE INDEX userunreadmessage_mid ON public.user_unread_message (mid);
CREATE INDEX userunreadmessage_uid ON public.user_unread_message (uid);
GRANT SELECT, INSERT, DELETE, UPDATE ON TABLE public.user_unread_message TO throat_be;
GRANT SELECT, INSERT, DELETE, UPDATE ON TABLE public.user_unread_message TO throat_py;

CREATE TABLE public.user_uploads (
  xid serial NOT NULL PRIMARY KEY,
  pid integer,
  uid character varying(40),
  fileid character varying(255),
  thumbnail character varying(255),
  status integer NOT NULL,
  FOREIGN KEY (pid) REFERENCES public.sub_post (pid),
  FOREIGN KEY (uid) REFERENCES public.user (uid)
);
CREATE INDEX useruploads_pid ON public.user_uploads (pid);
CREATE INDEX useruploads_uid ON public.user_uploads (uid);
GRANT SELECT, INSERT, DELETE, UPDATE ON TABLE public.user_uploads TO throat_be;
GRANT SELECT, INSERT, DELETE, UPDATE ON TABLE public.user_uploads TO throat_py;

CREATE TABLE public.visit (
  id bigserial NOT NULL PRIMARY KEY,
  datetime timestamp with time zone DEFAULT now() NOT NULL,
  ipaddr text NOT NULL
);
CREATE INDEX visit_datetime ON public.visit (datetime);
GRANT SELECT, INSERT, DELETE, UPDATE ON TABLE public.visit TO throat_be;
GRANT SELECT, INSERT, DELETE, UPDATE ON TABLE public.visit TO throat_py;

CREATE TABLE public.wiki (
  id serial NOT NULL PRIMARY KEY,
  is_global boolean NOT NULL,
  sid character varying(40),
  slug character varying(128) NOT NULL,
  title character varying(255) NOT NULL,
  content text NOT NULL,
  created timestamp without time zone NOT NULL,
  updated timestamp without time zone NOT NULL,
  FOREIGN KEY (sid) REFERENCES public.sub (sid)
);
CREATE INDEX wiki_sid ON public.wiki (sid);
CREATE UNIQUE INDEX wiki_sid_slug ON public.wiki (sid, slug);
GRANT SELECT, INSERT, DELETE, UPDATE ON TABLE public.wiki TO throat_be;
GRANT SELECT, INSERT, DELETE, UPDATE ON TABLE public.wiki TO throat_py;

INSERT INTO public.wiki (is_global, sid, slug, title, content, created, updated)
VALUES (true, NULL, 'tos', 'Terms of Service', 'Put your terms of service here', now(), now()),
       (true, NULL, 'privacy', 'Privacy Policy', 'Put your privacy policy here', now(), now()),
       (true, NULL, 'canary', 'Warrant canary', 'Put your canary here', now(), now()),
       (true, NULL, 'donate', 'Donate', 'Donation info goes here!', now(), now());


-- migrate:down
