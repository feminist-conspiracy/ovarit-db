-- migrate:up transaction:false

create unique index concurrently sub_post_vote_pid_uid
  on sub_post_vote (pid, uid);

-- migrate:down

drop index if exists sub_post_vote_pid_uid;
