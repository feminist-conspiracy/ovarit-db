let
  pkgs = import <nixpkgs> {};
in
pkgs.mkShell {
  buildInputs = [ pkgs.dbmate ];
  shellHook = ''
    export DATABASE_URL="postgres://throat:dev@127.0.0.1:5432/throat?sslmode=disable"
    export TEST_DATABASE_URL="postgres://throat:dev@127.0.0.1:5433/throat_test?sslmode=disable"
    export DBMATE_MIGRATIONS_DIR=./migrations
    export DBMATE_NO_DUMP_SCHEMA=1
  '';

}
